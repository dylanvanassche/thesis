# Thesis project website

A static onepager about my thesis: _Publishing real time Open Data for public transport_

- **Title**: Publishing real time Open Data for public transport
- **Author**: Dylan Van Assche
- **Year**: 2019
- **Supervisors**: Pieter Colpaert (IDLab) and Ann Philips (KU Leuven)


## Build status

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

